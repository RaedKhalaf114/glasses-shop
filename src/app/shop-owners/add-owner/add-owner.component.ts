import {Component, Inject} from '@angular/core';
import {Sidebar} from '../../shared/behavior/sidebar';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ShopsService} from '../../shops/services/shops.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'enigma-add-owner-component',
  styleUrls: ['add-owner.component.css'],
  templateUrl: 'add-owner.component.html'
})
export class AddOwnerComponent extends Sidebar {
  public form: FormGroup;
  public isSubmitting = false;
  isAddFromUsersActive = false;
  users;
  activeUser;

  constructor(
    @Inject(FormBuilder) private formBuilder: FormBuilder,
    private shopService: ShopsService,
    private messagesService: MessageService
  ) {
    super();
  }

  onSidebarInit() {
    this.initForm();
    this.initUsers();
  }

  private initUsers() {
    this.users = this.shopService.getUsers();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      imgURL: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      shopName: ['', Validators.required],
      shopImgURL: ['', Validators.required]
    });
  }

  onAddOwner() {
    this.isSubmitting = true;
    setTimeout(() => {
      this.isSubmitting = false;
      this.onHide();
      this.shopService.addOwner(this.form.getRawValue());
      this.messagesService.add({severity: 'success', summary: 'Owner added Successfully'});
    }, 2000);
  }

  onAddOwnerFromExistingUsers() {
    this.isAddFromUsersActive = !this.isAddFromUsersActive;
  }

  activateUser(user) {
    this.activeUser = user;
    this.form.controls['name'].setValue(user.name);
    this.form.controls['password'].setValue(user.password);
    this.form.controls['imgURL'].setValue(user.avatar);
    this.form.controls['email'].setValue(user.email);
    this.form.controls['name'].updateValueAndValidity();
    this.form.controls['password'].updateValueAndValidity();
    this.form.controls['imgURL'].updateValueAndValidity();
    this.form.controls['email'].updateValueAndValidity();
  }
}
