import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ShopsService} from '../shops/services/shops.service';
import {SideBarDirective} from '../shared/components/directives/side-bar.directive';
import {SidebarService} from '../shared/services/sidebar-service';
import {AddOwnerComponent} from './add-owner/add-owner.component';

@Component({
  selector: 'enigma-shop-owners-component',
  templateUrl: 'shop-owners.component.html',
  styleUrls: ['shop-owners.component.css']
})
export class ShopOwnersComponent implements OnInit, OnDestroy {
  public owners: ShopOwner[];
  @ViewChild(SideBarDirective) sidebar: SideBarDirective;
  public user;
  subscriptions = [];

  constructor(
    private shopsService: ShopsService,
    private sidebarService: SidebarService
  ) {}

  ngOnInit() {
    this.owners = this.shopsService.getOwners();
    this.user = JSON.parse(localStorage.getItem('user'));
    this.subscriptions.push(
      this.shopsService.onAddShop.subscribe(() => this.owners = this.shopsService.getOwners())
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(
      (subscription) => {
        subscription.unsubscribe();
      }
    );
  }

  public onAddOwner() {
    this.sidebarService.showSidebar(AddOwnerComponent, this.sidebar.viewContainerRef);
  }

  public removeUser(user) {
    this.owners = this.shopsService.removeOwner(user);
  }
}


export interface ShopOwner {
  name: string;
  imgURL: string;
  password: string;
  email: string;
  id: number;
}

