import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {SharedModule} from './shared/modules/shared.module';
import {APP_ROUTES} from './app.routes';
import {SharedComponentsModule} from './shared/components/components.index';
import {APP_GUARDS} from './shared/guards/guards.index';
import {INTERCEPTORS} from './shared/intercepters/interceptors.index';
import {APP_SERVICES} from './shared/services/services.index';
import {HttpClient} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {AuthenticationModule} from './login/authentication.module';
import {HomeModule} from './home/home.module';
import {ShopsModule} from './shops/shops.module';
import {ProfileComponent} from './profile/profile.component';
import {MyShopsComponent} from './home/my-shops/my-shops.component';
import {ShopOwnersComponent} from './shop-owners/shop-owners.component';
import {AddOwnerComponent} from './shop-owners/add-owner/add-owner.component';
import {SidebarModule} from 'primeng/primeng';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';

// TRANSLATION factory.
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const config = {
  apiKey: 'AIzaSyDBr2ArW1dQgM_xEx7NtMhA87cbubQCfvg',
  authDomain: 'glasses-shop-c5c15.firebaseapp.com',
  projectId: 'glasses-shop-c5c15',
};


const VIEW_MODULES = [
  AuthenticationModule,
  HomeModule,
  ShopsModule
];

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    MyShopsComponent,
    ShopOwnersComponent,
    AddOwnerComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule,
    SharedModule,
    APP_ROUTES,
    SidebarModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    SharedComponentsModule,

    ...VIEW_MODULES
  ],
  providers: [
    ...APP_GUARDS,
    ...INTERCEPTORS,
    ...APP_SERVICES
  ],
  entryComponents: [
    AddOwnerComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
