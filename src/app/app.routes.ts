import {RouterModule, Routes} from '@angular/router';
import {AuthenticationComponent} from './login/authentication.component';
import {AUTH_ROUTS} from './login/authentication.router';
import {HomeComponent} from './home/home.component';
import {HOME_ROUTES} from './home/routes/home.routes';
import {AuthGuard} from './shared/guards/auth.guard';
import {SessionGuard} from './shared/guards/session.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: HOME_ROUTES
  },
  {
    path: 'auth',
    component: AuthenticationComponent,
    canActivate: [SessionGuard],
    children: AUTH_ROUTS
  }
];

export const APP_ROUTES = RouterModule.forRoot(routes);
