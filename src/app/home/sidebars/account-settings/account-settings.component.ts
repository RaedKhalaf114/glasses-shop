import {Component} from '@angular/core';
import {Sidebar} from '../../../shared/behavior/sidebar';

@Component({
  selector: 'enigma-account-settings-sidebar-component',
  templateUrl: 'account-settings.component.html',
  styleUrls: ['account-settings.component.css']
})
export class AccountSettingsComponent extends Sidebar {
  onSidebarInit() {
  }

  public changeImage() {
  }

}
