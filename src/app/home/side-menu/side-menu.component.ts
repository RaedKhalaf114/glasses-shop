import {Component, Input, OnInit} from '@angular/core';
import {MenuItemModel} from '../models/menu-item.model';
import {HomeService} from '../services/home.service';

@Component({
  selector: 'enigma-side-menu-component',
  templateUrl: 'side-menu.component.html',
  styleUrls: ['side-menu.component.css']
})
export class SideMenuComponent implements OnInit {
  @Input('expandMenu') isMenuExpanded: boolean;

  public menuItems: MenuItemModel[];

  constructor(
    private homeService: HomeService
  ) {}

  ngOnInit() {
    this.menuItems = this.homeService.getMenuItems();
  }
}
