import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {ScreenShotService} from '../../shared/services/screen-shot.service';
import {BasketService} from '../../shared/services/basket.service';
import {AuthenticationService} from '../../shared/services/authentication.service';

@Component({
  selector: 'enigma-header-component',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy{
  /**
   *
   * @type {EventEmitter<any>}
   */
  @Output() toggle: EventEmitter<{type: 'menu' | 'settings'}> = new EventEmitter();

  basketProducts = [];
  screenshots = [];
  subscriptions = [];

  isAtentionToBasketActive;
  isAtentionToScreenshotActive;
  user;

  constructor(
    private basketService: BasketService,
    private screenshotService: ScreenShotService,
    private authService: AuthenticationService
  ) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.screenshots = this.screenshotService.getScreenShots();
    this.basketProducts = this.basketService.getBasketItems();
    this.subscriptions.push(this.basketService.onBasketItemsChange.subscribe(
      (products) => {
        this.basketProducts = products;
        this.isAtentionToBasketActive = true;
        setTimeout(() => this.isAtentionToBasketActive = false, 500);
      }
    ));
    this.subscriptions.push(this.screenshotService.onScreenshotsChange.subscribe(
      (screenShots: any) => {
        this.screenshots = screenShots.filter(screen => screen.user.email  === this.authService.getUser().email );
        this.isAtentionToScreenshotActive = true;
        setTimeout(() => this.isAtentionToScreenshotActive = false, 500);
      }
    ));
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.forEach(
        (subscription) => {
          subscription.unsubscribe();
        }
      );
    }
  }


  public onToggleMenu() {
    this.toggle.emit({type: 'menu'});
  }

  public onToggleSettings() {
    this.toggle.emit({type: 'settings'});
  }

  public onToggle(type) {
    this.toggle.emit({type});
  }

  public logout() {
    this.authService.logout();
  }
}
