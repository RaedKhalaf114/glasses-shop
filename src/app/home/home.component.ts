import {Component, OnInit, ViewChild} from '@angular/core';
import {FADE_ANIMATION} from '../shared/animations/fade-animation';
import {Router} from '@angular/router';
import {SideBarDirective} from '../shared/components/directives/side-bar.directive';
import {SidebarService} from '../shared/services/sidebar-service';
import {AccountSettingsComponent} from './sidebars/account-settings/account-settings.component';
import {ScreenShotService} from '../shared/services/screen-shot.service';
import {BasketService} from '../shared/services/basket.service';
import {ScreenShotsComponent} from '../shops/screen-shots/screen-shots.component';
import {BasketComponent} from '../shops/basket/basket.component';

@Component({
  selector: 'enigma-home-component',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
  animations: [FADE_ANIMATION]
})
export class HomeComponent implements OnInit {
  /**
   * Track the appearance of SideMenu.
   * @type {boolean}
   */
  public displaySideMenu = false;

  /**
   * Controls the appearance of menu labels.
   * and app-display margin.
   * @type {boolean}
   */
  public isMenuExpanded = false;

  /**
   *
   */
  @ViewChild(SideBarDirective) sidebar: SideBarDirective;

  constructor(
    private router: Router,
    private sidebarService: SidebarService,
    private screenshotService: ScreenShotService,
    private basketService: BasketService
  ) {}


  ngOnInit() {
    this.trackRouterChange();
  }

  private trackRouterChange() {
    //  TODO distroy Router subscription.
    this.router.events.subscribe(
      () => {
        this.displaySideMenu = false;
      }
    );
  }

  /**
   * When the Burger Icon triggers the appearance of sideMenu.
   */
  public onTriggerMenu() {
    this.isMenuExpanded = !this.isMenuExpanded;
  }

  public onTriggerSettings() {
    this.sidebarService.showSidebar(AccountSettingsComponent, this.sidebar.viewContainerRef);
  }

  public onTriggerSidebar(componentRef) {
    this.sidebarService.showSidebar(componentRef, this.sidebar.viewContainerRef);
  }

  /**
   * Track the header actions.
   * @param {{type: "menu" | "settings"}} $event
   */
  public onHeaderAction($event: {type: 'menu' | 'settings' | 'screenShots' | 'basket'}) {
    switch ($event.type) {
      case 'menu':
        this.onTriggerMenu();
        break;
      case 'settings':
        this.onTriggerSettings();
        break;
      case 'screenShots':
        this.onTriggerSidebar(ScreenShotsComponent);
        break;
      case 'basket':
        this.onTriggerSidebar(BasketComponent);
        break;
    }
  }
}
