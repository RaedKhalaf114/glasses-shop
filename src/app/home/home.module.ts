import {NgModule} from '@angular/core';
import {HomeComponent} from './home.component';
import {SharedModule} from '../shared/modules/shared.module';
import {SharedComponentsModule} from '../shared/components/components.index';
import {RouterModule} from '@angular/router';
import {SidebarModule} from 'primeng/primeng';
import {SideMenuComponent} from './side-menu/side-menu.component';
import {HeaderComponent} from './header/header.component';
import {HomeService} from './services/home.service';
import {AccountSettingsComponent} from './sidebars/account-settings/account-settings.component';
import {ToastModule} from 'primeng/toast';

@NgModule({
  declarations: [
    HomeComponent,
    SideMenuComponent,
    HeaderComponent,
    AccountSettingsComponent
  ],
  imports: [
    SharedModule,
    SharedComponentsModule,
    RouterModule,
    SidebarModule,
    ToastModule
  ],
  exports: [
    HomeComponent
  ],
  providers: [
    HomeService
  ],
  entryComponents: [
    AccountSettingsComponent
  ]
})
export class HomeModule {}
