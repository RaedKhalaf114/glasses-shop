import {Injectable} from '@angular/core';
import {MenuItemModel} from '../models/menu-item.model';

@Injectable()
export class HomeService {
  public getMenuItems(): MenuItemModel[] {
    const user = JSON.parse(localStorage.getItem('user'));
    const basicItems = [
      {
        label: 'Shops',
        icon: 'fas fa-store',
        routerLink: 'shops'
      },
      {
        label: 'Profile',
        icon: 'fas fa-user',
        routerLink: 'profile'
      }
    ];
    if (user.role === 'owner') {
      // basicItems.push(
      //   {
      //     label: 'My Shops',
      //     icon: 'fas fa-store',
      //     routerLink: 'my-shops'
      //   }
      // );
    } else if (user.role === 'admin') {
      basicItems.push({
        label: 'Shop Owners',
        icon: 'fas fa-users',
        routerLink: 'shop-owners'
      });
    }
    return basicItems;
  }
}
