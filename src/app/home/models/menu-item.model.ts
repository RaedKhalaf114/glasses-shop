export interface MenuItemModel {
  label: string;
  icon: string;
  routerLink: string;
}
