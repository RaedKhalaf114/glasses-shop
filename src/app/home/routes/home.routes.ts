import {Routes} from '@angular/router';
import {ShopsComponent} from '../../shops/shops.component';
import {ShopDetailsComponent} from '../../shops/shop-details/shop-details.component';
import {ProfileComponent} from '../../profile/profile.component';
import {MyShopsComponent} from '../my-shops/my-shops.component';
import {ShopOwnersComponent} from '../../shop-owners/shop-owners.component';

export const HOME_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'shops',
    pathMatch: 'full'
  },
  {
    path: 'shops',
    component: ShopsComponent
  },
  {
    path: 'shops-details',
    component: ShopDetailsComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'my-shops',
    component: MyShopsComponent
  },
  {
    path: 'shop-owners',
    component: ShopOwnersComponent
  }
];
