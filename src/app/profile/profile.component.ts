import {Component, OnInit} from '@angular/core';
import {BasketService} from '../shared/services/basket.service';
import {ScreenShotService} from '../shared/services/screen-shot.service';

@Component({
  selector: 'enigma-profile-component',
  styleUrls: ['profile.component.css'],
  templateUrl: 'profile.component.html'
})
export class ProfileComponent implements OnInit {
  public user;
  public basketSize = 0;
  public screenShotsCount = 0;

  constructor(
    private basketService: BasketService,
    private screenShotService: ScreenShotService
  ) {}


  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.basketSize = this.basketService.getBasketItems().length;
    this.screenShotsCount = this.screenShotService.getScreenShots().length;
  }
}
