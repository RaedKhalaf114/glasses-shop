import {OnDestroy} from '@angular/core';

export class SubscriptionTracker implements OnDestroy {
    /**
     * The list of subscriptions tracked.
     */
    subscriptions: any[] = [];

    /**
     * Abstract method, used to unsubscribe the subscription.
     */
    destroySubscriptions() {
        this.subscriptions.forEach(
            (subscription) => {
                subscription.unsubscribe();
        });
    }

    addSubscription(subsctiption) {
        this.subscriptions.push(subsctiption);
    }

    ngOnDestroy() {
        this.destroySubscriptions();
    }
}
