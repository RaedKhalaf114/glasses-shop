export enum SpinnerState {
    LOADING = 0,
    EMPTY = 1,
    FAILED = 2,
    LOADED = 3
}
