import {Input, OnInit} from '@angular/core';

/**
 * Shared properties between the SideBar components.
 */
export abstract class Sidebar implements OnInit {
  public displaySidebar = false;

  /**
   * on Hide the account settings sidebar.
   * notify the parent to remove it from the DOM.
   */
  @Input() onHide: any;

  ngOnInit() {
    this.showSidebar();
    this.onSidebarInit();
  }

  private showSidebar() {
    setTimeout(() => {
      this.displaySidebar = true;
    }, 0);
  }

  public onClose(msg?) {
    // remove this component from the DOM after the animation finishes.
    setTimeout(() => {
      this.onHide(msg);
    }, 200);
  }

  public abstract onSidebarInit();
}
