export interface UserModel {
  name: string;
  email: string;
  department: string;
  position: string;
  mobileNumber: string;
  homePage: string;
}
