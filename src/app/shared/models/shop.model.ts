import {ProductModel} from './product.model';
import {ShopOwner} from '../../shop-owners/shop-owners.component';

export interface ShopModel {
  name: string;
  backgroundImage: any;
  products: ProductModel[];
  owner: ShopOwner;
}
