export interface ProductModel {
  name: string;
  owner: string;
  description: string;
  imgURL: any;
  price: number;
  rating: number;
  productId: string;
}
