export interface AccountGroupModel extends GroupModel {
  subGroups: GroupModel[];

//  View variables.
  isExpanded?: boolean;
}

interface GroupModel {
  name: string;
  profilesCount: number;
  subgroupsCount: number;
}
