export interface TableColModel {
  header: string;
  field: string;
}
