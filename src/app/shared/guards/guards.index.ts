import {AuthGuard} from './auth.guard';
import {SessionGuard} from './session.guard';

export const APP_GUARDS = [
  AuthGuard,
  SessionGuard
];
