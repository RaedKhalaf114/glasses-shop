import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {environment} from '../../../environments/environment';
import {MessageService} from 'primeng/api';
import {AlertService} from '../services/alert.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    public apiURL = environment.API_BASE_URL;

    constructor(
        private router: Router,
        private auth: AuthenticationService,
        private messageService: MessageService,
        private alertService: AlertService
    ) {}

    /**
     * @param HttpRequest<any> request - The intercepted request
     * @param HttpHandler next - The next interceptor in the pipeline
     * @return Observable<HttpEvent<any>>
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = this.addToken(request);
        return next.handle(request)
        // add error handling
            .pipe(
                catchError(
                    (error: any, caught: Observable<HttpEvent<any>>) => {
                        if (error.status === 401) {
                            this.handleAuthError();
                            // if you've caught / handled the error, you don't
                            // want to rethrow it unless you also want
                            // downstream consumers to have to handle it as
                            // well.
                            return of(error);
                        } else {
                          this.alertService.alertError(error);
                        }
                        throw error;
                    }
                ),
            );
    }

    /**
     * Handle API authentication errors.
     */
    private handleAuthError() {
        // TODO clear stored credentials; they're invalid
        // navigate back to the login page
        this.router.navigate(['/login']);
    }

    /**
     * Add stored auth token to request headers.
     * @param HttpRequest<any> request - the intercepted request
     * @return HttpRequest<any> - the modified request
     */
    private addToken(request: HttpRequest<any>): HttpRequest<any> {
        const newOptions = {
            url: request.url,
            setHeaders: {
                'Authorization-Token': ''
            }
        };

        // add server prefix for API calls only.
        if (request.url.startsWith(':')) {
            newOptions.url = this.getBaseUrl(request.url);
        }

        const token: string = this.auth.getUserAuthToken();
        if (token) {
            newOptions.setHeaders['Authorization-Token'] = `${token}`;
        }

        return request.clone(newOptions);
    }

    /**
     * Attach the server API URL to each request.
     * @param url
     */
    private getBaseUrl(url) {
        return this.apiURL + url.substring(1);
    }
}
