import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {AuthenticationService} from './authentication.service';

@Injectable()
export class BasketService {
  public basketItems;
  public onBasketItemsChange: Subject<any> = new Subject();

  constructor(
    private authService: AuthenticationService
  ) {}


  public getBasketItems() {
    let basketItems = JSON.parse(localStorage.getItem('basket'));
    if (!basketItems) {
      basketItems = [];
    }
    this.basketItems = basketItems;
    return this.basketItems.filter((basketItem) => basketItem.user.email === this.authService.getUser().email);
  }


  public addToBasket(product) {
    const basketItems = this.getBasketItems();
    product['user'] = this.authService.getUser();
    basketItems.push(product);
    localStorage.setItem('basket', JSON.stringify(basketItems));
    this.onBasketItemsChange.next(basketItems);
  }

  public removeBasketProduct(index) {
    const basketProducts = [...this.getBasketItems()];
    basketProducts.splice(index, 1);
    localStorage.setItem('basket', JSON.stringify(basketProducts));
    this.onBasketItemsChange.next(basketProducts);
  }

  public emptyBasket() {
    this.basketItems = [];
    localStorage.setItem('basket', '[]');
    this.onBasketItemsChange.next([]);
  }
}
