import {AuthenticationService} from './authentication.service';
import {UtilitiesService} from './utilities.service';
import {StorageService} from './storage/storage.service';
import {UserService} from './user.service';
import {MessageService} from 'primeng/api';
import {AlertService} from './alert.service';
import {SidebarService} from './sidebar-service';
import {LanguageService} from './language.service';
import {ToolbarService} from './toolbar.service';
import {CommunicationService} from './communication.service';
import {ScreenShotService} from './screen-shot.service';
import {BasketService} from './basket.service';

export const APP_SERVICES = [
  AuthenticationService,
  UtilitiesService,
  StorageService,
  UserService,
  MessageService,
  AlertService,
  SidebarService,
  LanguageService,
  ToolbarService,
  CommunicationService,
  ScreenShotService,
  BasketService
];
