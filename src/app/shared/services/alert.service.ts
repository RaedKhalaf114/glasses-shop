import {Injectable, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MessageService} from 'primeng/api';

@Injectable()
export class AlertService implements OnInit {
    private generalErrorMessage;

    constructor(
        private translateService: TranslateService,
        private messagesService: MessageService
    ) {}

    ngOnInit() {
        this.translateService.get(
            ['ALERT.ERROR_TITLE', 'ALERT.SUCCESS_TITLE', 'ALERT.GENERAL_ERROR', 'ALERT.GENERAL_SUCCESS']
        )
            .subscribe(
                (data) => {
                    console.log(data);
                }
            );
    }

    public alertError(error) {
        const errorMessage = error.error.error ? error.error.error : this.generalErrorMessage;
        this.messagesService.add({severity: 'error', detail: errorMessage});
    }
}
