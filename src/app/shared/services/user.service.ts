import {Injectable} from '@angular/core';
import {StorageService} from './storage/storage.service';
import {STORAGE_KEYS} from './storage/storage.keys';
import {SHARED_CONSTANTS} from './shared_constants/constants';

@Injectable()
export class UserService {

  constructor(
    private storageService: StorageService
  ) {}
}
