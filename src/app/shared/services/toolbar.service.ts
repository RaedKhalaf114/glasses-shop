import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {BreadCrumbsModel} from '../components/tools-bar/models/bread-crumbs.model';

@Injectable()
export class ToolbarService {
  /**
   * Notify the Toolbar with any change in the secondary breadcrumbs.
   * secondary breadcrumbs is a steps which is not depending on the active route.
   * it depends on something changed inside the component.
   */
  public secondaryBreadCrumbs$ = new Subject<{crumbs: BreadCrumbsModel[], resetToRoot: any}>();
}
