import {ComponentFactoryResolver, Injectable} from '@angular/core';
import {FloatingButtonComponent} from '../components/floating-button/floating-button.component';
import {flatMap} from 'tslint/lib/utils';
import {CommunicationService} from './communication.service';
import {MessageService} from 'primeng/api';

@Injectable()
export class SidebarService {

  constructor(
    private componentFactory: ComponentFactoryResolver,
    private communicationService: CommunicationService,
    private messageService: MessageService
  ) {}

  /**
   * create the component factory and manage
   * the show/hide operations of each sidebar.
   * @param: component: which component to be showed.
   * @container: the container which contains the component.
   */
  public showSidebar(component, container) {
    const componentFactory = this.componentFactory.resolveComponentFactory(component);

    const containerRef = container;
    containerRef.clear();

    const componentRef = containerRef.createComponent(componentFactory);

    this.broadcastSidebarTrigger(true);
    (<any>componentRef.instance).onHide = (msg?) => {
      containerRef.clear();
      if (msg) {
        this.messageService.add(msg);
      }
      this.broadcastSidebarTrigger(false);
    };
  }

  private broadcastSidebarTrigger(value: boolean) {
    this.communicationService.onTriggerSidebar.next(value);
  }
}
