import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {ProductModel} from '../models/product.model';
import {AuthenticationService} from './authentication.service';

@Injectable()
export class ScreenShotService {
  public screenshots: {imgSrc: string, product: ProductModel, user: any}[] = [];
  public onScreenshotsChange: Subject<{imgSrc: string, product: ProductModel}[]> = new Subject();

  constructor(
    private authService: AuthenticationService
  ) {
    this.initScreenshots();
  }

  public storeScreenShot(screen, broadcast = true) {
    let sessionScreens = JSON.parse(sessionStorage.getItem('images'));
    let localScreens = JSON.parse(localStorage.getItem('images'));
    if (!sessionScreens) {
      sessionScreens = [];
      sessionStorage.setItem('images', JSON.stringify([]));
    }
    if (!localScreens) {
      localScreens = [];
      localStorage.setItem('images', JSON.stringify([]));
    }

    try {
      sessionScreens.push(screen);
      sessionStorage.setItem('images', JSON.stringify(sessionScreens));
    } catch (e) {
      localScreens.push(screen);
      localStorage.setItem('images', JSON.stringify(localScreens));
    }
    this.screenshots = [...JSON.parse(sessionStorage.getItem('images')), ...JSON.parse(localStorage.getItem('images'))];
    if (broadcast) {
      this.onScreenshotsChange.next(this.screenshots);
    }
  }

  public initScreenshots() {
    let sessionScreens = JSON.parse(sessionStorage.getItem('images'));
    let localScreens = JSON.parse(localStorage.getItem('images'));
    if (!sessionScreens) {
      sessionScreens = [];
      sessionStorage.setItem('images', JSON.stringify([]));
    }
    if (!localScreens) {
      localScreens = [];
      localStorage.setItem('images', JSON.stringify([]));
    }

    this.screenshots = [...sessionScreens, ...localScreens];
  }

  public getScreenShots() {
    this.initScreenshots();
    return this.screenshots.filter(screenShot => screenShot.user.email === this.authService.getUser().email);
  }

  public removeScreenshot(index) {
    const screenShots = [...this.getScreenShots()];
    screenShots.splice(index, 1);
    this.replaceScreenShots(screenShots);
    this.getScreenShots();
    this.onScreenshotsChange.next(screenShots);
  }

  private replaceScreenShots(screens) {
    localStorage.setItem('images', JSON.stringify([]));
    sessionStorage.setItem('images', JSON.stringify([]));
    screens.forEach((screen) => {
      this.storeScreenShot(screen, false);
    });
  }
}
