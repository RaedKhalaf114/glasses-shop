import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {SelectInputOptionModel} from '../components/select-input/models/select-input.model';
import {BehaviorSubject} from 'rxjs';
import {STORAGE_KEYS} from './storage/storage.keys';
import {SHARED_CONSTANTS} from './shared_constants/constants';
import {StorageService} from './storage/storage.service';

@Injectable()
export class LanguageService {
  public $language: BehaviorSubject<{lang: string}>;

  constructor(
    private translationService: TranslateService,
    private storageService: StorageService
  ) {}

  /**
   *
   * @param initialLang
   */
  private initiateLangSubscription(initialLang) {
    if (!this.$language) {
      this.$language = new BehaviorSubject<{lang: string}>({lang: initialLang});
    }
  }

  /**
   * Return array of supported languages.
   * @return {SelectInputOptionModel[]}
   */
  public getSupportedLanguages(): SelectInputOptionModel[] {
    return [
      {
        label: 'English',
        value: 'en'
      },
      {
        label: 'Arabic',
        value: 'ar'
      }
    ];
  }

  /**
   * 1. first language.
   * 2. body direction.
   * 3. initialize language subscription.
   */
  public initLanguage() {
    const currentLang = this.getCurrentLanguage();
    this.initiateLangSubscription(currentLang);
    this.use(currentLang);
  }

  /**
   * use the argument lang. 'en' | 'ar'
   * @param {string} lang
   */
  public use(lang: string) {
    this.translationService.use(lang);
    switch (lang) {
      case 'en':
        window.document.body.classList.add('ltr');
        window.document.body.classList.remove('rtl');
        break;
      case 'ar':
        window.document.body.classList.add('rtl');
        window.document.body.classList.remove('ltr');
        break;
    }
    this.storageService.set(STORAGE_KEYS.LANGUAGE, lang);
    this.$language.next({lang});
  }

  public getCurrentLanguage() {
    const language = this.storageService.get(STORAGE_KEYS.LANGUAGE);
    if (language) {
      return language;
    } else {
      this.storageService.set(STORAGE_KEYS.LANGUAGE, SHARED_CONSTANTS.DEFAULT_LANG);
      return SHARED_CONSTANTS.DEFAULT_LANG;
    }
  }
}
