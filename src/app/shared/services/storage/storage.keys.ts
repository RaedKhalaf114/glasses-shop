export const STORAGE_KEYS = {
    LANGUAGE: 'lang',
    LANGUAGES: {
        ENGLISH: 'en',
        ARABIC: 'ar'
    },
    TOKEN_KEY: 'token',
};
