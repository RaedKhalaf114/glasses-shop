import {Injectable} from '@angular/core';
import {STORAGE_KEYS} from './storage.keys';

@Injectable()
export class StorageService {
    public set(key, value) {
        localStorage.setItem(key, value);
    }

    public get(key) {
        return localStorage.getItem(key);
    }

    public remove(key) {
        localStorage.removeItem(key);
    }
}
