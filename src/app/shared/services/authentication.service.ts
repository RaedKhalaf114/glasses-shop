import {Injectable} from '@angular/core';
import {StorageService} from './storage/storage.service';
import {STORAGE_KEYS} from './storage/storage.keys';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {APIS} from './shared_constants/apis';

@Injectable()
export class AuthenticationService {

    constructor(
        private storageService: StorageService,
        private router: Router,
        private http: HttpClient
    ) {}

    /**
     *
     */
    public getUserAuthToken() {
        return this.storageService.get(STORAGE_KEYS.TOKEN_KEY);
    }

    /**
     *
     * @param token
     */
    public setUserAuthToken(token) {
        this.storageService.set(STORAGE_KEYS.TOKEN_KEY, token);
    }

    /**
     *
     * @return {boolean}
     */
    public isUserActive(): boolean {
        return this.getUserAuthToken() ? true : false;
    }

    /**
     * clear the information related to the customer.
     */
    public removeUserCredintials() {
        this.storageService.remove(STORAGE_KEYS.TOKEN_KEY);
    }

    public logout() {
      this.removeUserCredintials();
      this.router.navigate(['auth/login']);
    }

    public setUser(user) {
      localStorage.setItem('user', JSON.stringify(user));
    }

    public addUser(user) {
      let users = JSON.parse(localStorage.getItem('users'));
      if (users && users instanceof Array) {
        users.push({
          role: 'user',
          name: user.name,
          email: user.email,
          password: user.password,
          avatar: user.imgURL,
          id: Math.random() * 10000000000
        });
      } else {
        users = [
          {
            role: 'user',
            name: user.name,
            email: user.email,
            password: user.password,
            avatar: user.imgURL,
            id: Math.random() * 10000000000
          }
        ];
      }
      localStorage.setItem('users', JSON.stringify(users));
      localStorage.setItem('user', JSON.stringify(user));
    }

    public getUser() {
      return JSON.parse(localStorage.getItem('user'));
    }
}
