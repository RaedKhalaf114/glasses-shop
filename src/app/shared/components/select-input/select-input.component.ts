import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {SelectInputOptionModel} from './models/select-input.model';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'enigma-select-input-component',
  templateUrl: 'select-input.component.html',
  styleUrls: ['select-input.component.css']
})
export class SelectInputComponent {
  // Inputs
  @Input() options: SelectInputOptionModel[];
  @Input() selected: any;
  @Input('control') formControl: FormControl;
  @Input() label: string;

  // Outputs
  @Output() onChange = new EventEmitter<any>();
}
