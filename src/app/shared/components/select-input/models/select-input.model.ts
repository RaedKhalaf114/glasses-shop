export interface SelectInputOptionModel {
  label: string;
  value: string;
}
