import {Component} from '@angular/core';

@Component({
  selector: 'enigma-search-bar-component',
  templateUrl: 'search-bar.component.html',
  styleUrls: ['search-bar.component.css']
})
export class SearchBarComponent {
}
