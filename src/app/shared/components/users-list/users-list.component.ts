import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {UserModel} from '../../models/user.model';

@Component({
  selector: 'enigma-users-list-component',
  templateUrl: 'users-list.component.html',
  styleUrls: ['users-list.component.css']
})
export class UsersListComponent implements OnInit, OnChanges {
  /**
   *
   */
  @Input() users: UserModel[];

  /**
   *
   * @type {any[]}
   */
  public columns = [];

  constructor() {}

  ngOnInit() {
  }

  ngOnChanges() {
    this.initColumns();
  }

  private initColumns() {
    this.columns = [
      {field: 'name', header: 'Name'},
      {field: 'email', header: 'Email'},
      {field: 'department', header: 'Department'},
      {field: 'position', header: 'Position'},
      {field: 'mobileNumber', header: 'Mobile Number'},
      {field: 'homePage', header: 'Home Page'}
    ];
  }
}
