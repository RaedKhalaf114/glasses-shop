import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'enigma-classic-input-component',
  templateUrl: 'classic-input-field.component.html',
  styleUrls: ['classic-input-field.component.css']
})
export class ClassicInputFieldComponent {
  // Inputs
  @Input() label: string;
  @Input() type: string;
  @Input('control') formControl: FormControl;

  // Outputs
  @Output() enter = new EventEmitter();

  /**
   * Notify Parent if the enter is pressed
   * to submit the form.
   * @param keyCode
   */
  public onKeypress(keyCode) {
    // 13 is the enter keyCode
    if (keyCode === 13) {
      this.enter.emit();
    }
  }
}
