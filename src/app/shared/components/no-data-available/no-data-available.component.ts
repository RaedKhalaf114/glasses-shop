import {Component} from '@angular/core';

@Component({
    selector: 'enigma-no-data-available-component',
    templateUrl: 'no-data-available.component.html',
    styleUrls: ['no-data-available.component.css']
})
export class NoDataAvailableComponent {}
