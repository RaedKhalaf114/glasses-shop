import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'enigma-floating-button',
  templateUrl: 'floating-button.component.html',
  styleUrls: ['floating-button.component.css']
})
export class FloatingButtonComponent {
  // INPUTS
  @Input() icon: string;

  // OUTPUTS
  @Output() trigger: EventEmitter<any> = new EventEmitter<any>();

}
