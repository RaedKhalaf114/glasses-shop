import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {BreadCrumbsModel} from './models/bread-crumbs.model';
import {NavigationStart, ResolveEnd, Router} from '@angular/router';
import {ToolbarService} from '../../services/toolbar.service';

@Component({
  selector: 'enigma-tools-bar-component',
  templateUrl: 'tools-bar.component.html',
  styleUrls: ['tools-bar.component.css']
})
export class ToolsBarComponent implements OnInit, OnDestroy {
  /**
   *
   * @type {EventEmitter<{content: string}>}
   */
  @Output() search: EventEmitter<{content: string}> = new EventEmitter<{content: string}>();

  /**
   *
   */
  public breadCrumbs: BreadCrumbsModel[];
  public breadCrumbsURL: string;

  /**
   * Secondary bread crumbs is provided by the active component.
   * its not depending on the active route.
   * it depends on something inside the component itself.
   */
  public secondaryBreadCrumbs: BreadCrumbsModel[] = [];

  /**
   * Callback coming from the component which care about secondary Crumbs.
   * this callBack will reset the crumbs to the root in the component.
   */
  public resetSecondaryCrumbsToRoot: any;

  /**
   * to be destroyed later on.
   */
  public subscriptions = [];

  constructor(
    private router: Router,
    private toolbarService: ToolbarService
  ) {}

  ngOnInit() {
    this.initBreadCrumbs();
  }

  private initBreadCrumbs() {
    this.analyzeRoute();
    this.listenToRouteChange();
    this.listenToSecondaryBreadCrumbs();
  }

  private analyzeRoute(url = this.router.url) {
    if (this.breadCrumbsURL === url) {
      return;
    }

    this.breadCrumbsURL = url;

    // Split the URL upon /.
    // and remove the first part since its not needed in the crumbs.
    const crumbs = this.breadCrumbsURL.split('/').slice(2);

    // reset the crumbs array.
    this.breadCrumbs = [];

    crumbs.forEach(
      (crumb) => {
        const title = crumb.substring(0, 1).toUpperCase() + crumb.substring(1).toLowerCase();
        const crumbModel = {label: title, link: crumb};
        this.breadCrumbs.push(crumbModel);
      }
    );
  }

  private listenToRouteChange() {
    const subscription = this.router.events
      .subscribe(
      (event) => {
        if (event instanceof NavigationStart) {
          this.analyzeRoute(event.url);
        }
      }
    );
    this.subscriptions.push(subscription);
  }

  /**
   * Listen and reflect any change in the secondary breadCrumbs.
   */
  private listenToSecondaryBreadCrumbs() {
    const subscription = this.toolbarService.secondaryBreadCrumbs$.subscribe(
      (data: {crumbs: BreadCrumbsModel[], resetToRoot: any}) => {
        this.secondaryBreadCrumbs = data.crumbs;
        this.resetSecondaryCrumbsToRoot = data.resetToRoot;
      }
    );
  }

  ngOnDestroy() {
    // destroy subscription.
    this.subscriptions.forEach($sub => $sub.unsubscribe());
  }

  public navigateTo(breadCrumb: BreadCrumbsModel) {
    if (breadCrumb.link) {

      // Check if the no need for navigation.
      // if URL ends with the view need to be directed to and there are no secondary breadcrumbs no need fo re-rendering.
      if (this.breadCrumbsURL.endsWith(breadCrumb.link) && this.secondaryBreadCrumbs.length === 0) {
        return;
      }

      // get the breadcrumbs before the navigated one, to attach it in the navigate router method.
      const prefixURL = this.breadCrumbsURL.substr(0, this.breadCrumbsURL.indexOf(breadCrumb.link));
      this.router.navigate([prefixURL + breadCrumb.link]);
      this.resetSecondaryCrumbs();
    } else {
      breadCrumb.command();
    }
  }

  /**
   * notify the component which care about the secondary that the crumbs is gone.
   * in order to reset its configuration to scratch.
   */
  public resetSecondaryCrumbs() {
    if (this.secondaryBreadCrumbs.length > 0 && this.resetSecondaryCrumbsToRoot) {
      this.resetSecondaryCrumbsToRoot();
      this.secondaryBreadCrumbs = [];
    }
  }

  public getBreadCrumbs() {
    return [...this.breadCrumbs, ...this.secondaryBreadCrumbs];
  }
}
