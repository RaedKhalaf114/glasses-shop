
export interface BreadCrumbsModel {
  label: string;
  link?: string;
  command?: any;
}
