import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'enigma-action-button-component',
  templateUrl: 'action-button.component.html',
  styleUrls: ['action-button.component.css']
})
export class ActionButtonComponent {
  // Inputs
  @Input() type: 'primary' | 'secondary' | 'danger';
  @Input() label: string;
  @Input() isLoading: boolean;
  @Input() isDisabled: boolean;
  @Input() severity: 'success' | 'error' | 'cancel' = 'success';

  // Outputs
  @Output() trigger = new EventEmitter();

  public onTriggerAction() {
    if (this.isLoading || this.isDisabled) {
      return;
    }
    this.trigger.emit();
  }
}
