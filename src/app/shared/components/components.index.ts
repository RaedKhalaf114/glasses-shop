import {SharedModule} from '../modules/shared.module';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CheckboxModule, DropdownModule, InputTextModule} from 'primeng/primeng';

import {NoDataAvailableComponent} from './no-data-available/no-data-available.component';
import {PermissionDirective} from './directives/permission.directive';
import {InputFieldComponent} from './input-field/input-field.component';
import {SideBarDirective} from './directives/side-bar.directive';
import {SelectInputComponent} from './select-input/select-input.component';
import {ClassicInputFieldComponent} from './classic-input-field/classic-input-field.component';
import {ActionButtonComponent} from './action-button/action-button.component';
import {ToolsBarComponent} from './tools-bar/tools-bar.component';
import {SearchBarComponent} from './search-bar/search-bar.component';
import {FloatingButtonComponent} from './floating-button/floating-button.component';
import {UsersListComponent} from './users-list/users-list.component';
import {TableModule} from 'primeng/table';

const SHARED_COMPONENTS = [
  NoDataAvailableComponent,
  InputFieldComponent,
  SelectInputComponent,
  ClassicInputFieldComponent,
  ActionButtonComponent,
  FloatingButtonComponent,
  SearchBarComponent,
  ToolsBarComponent,
  UsersListComponent
];

const SHARED_DIRECTIVES = [
  PermissionDirective,
  SideBarDirective
];

@NgModule({
  declarations: [
    ...SHARED_COMPONENTS,
    ...SHARED_DIRECTIVES
  ],
  exports: [
    ...SHARED_COMPONENTS,
    ...SHARED_DIRECTIVES
  ],
  imports: [
    RouterModule,
    SharedModule,
    InputTextModule,
    DropdownModule,
    TableModule,
    CheckboxModule
  ]
})
export class SharedComponentsModule {
}
