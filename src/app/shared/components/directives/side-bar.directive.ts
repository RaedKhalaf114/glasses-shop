import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[enigmaSideBar]'
})
export class SideBarDirective {
  constructor(
    public viewContainerRef: ViewContainerRef
  ) {}
}
