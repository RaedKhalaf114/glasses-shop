import {Component, Input} from '@angular/core';
import {DialogService} from 'primeng/api';
import {ProductWearComponent} from '../product-wear/product-wear.component';
import {ProductModel} from '../../shared/models/product.model';
import {BasketService} from '../../shared/services/basket.service';
import {ShopsService} from '../services/shops.service';

@Component({
  selector: 'enigma-product-item-component',
  templateUrl: 'product.component.html',
  styleUrls: ['product.component.css']
})
export class ProductComponent {
  @Input() product: ProductModel;

  constructor(
    public dialogService: DialogService,
    public basketService: BasketService,
    public shopsService: ShopsService
  ) {}

  tryProduct() {
    this.shopsService.activeProduct = this.product;
    const ref = this.dialogService.open(ProductWearComponent, {
      header: 'Open the camera to wear the product'
    });
  }

  addToBasket() {
    this.basketService.addToBasket(this.product);
  }
}
