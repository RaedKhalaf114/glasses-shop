import {ChangeDetectorRef, Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ShopModel} from '../shared/models/shop.model';
import {ShopsService} from './services/shops.service';
import {FADE_ANIMATION} from '../shared/animations/fade-animation';
import {Router} from '@angular/router';
import {SideBarDirective} from '../shared/components/directives/side-bar.directive';
import {ImageSliderComponent} from './image-slider/image-slider.component';
import {SidebarService} from '../shared/services/sidebar-service';
import {AddShopComponent} from './add-shop/add-shop.component';
import {AccountSettingsComponent} from '../home/sidebars/account-settings/account-settings.component';

@Component({
  selector: 'enigma-shops-component',
  templateUrl: 'shops.component.html',
  styleUrls: ['shops.component.css'],
  animations: [FADE_ANIMATION]
})
export class ShopsComponent  implements OnInit, OnDestroy {
  public shops: ShopModel[];
  public isLoading = false;
  private subscriptions = [];
  public user;
  @ViewChild(SideBarDirective) sidebar: SideBarDirective;

  constructor(
    private shopsService: ShopsService,
    private router: Router,
    private sidebarService: SidebarService
  ) {}

  ngOnInit() {
    this.initShops();
    this.lsnToShops();
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  private lsnToShops() {
    this.subscriptions.push(
      this.shopsService.onAddShop.subscribe(
        () => {
          this.initShops();
        }
      )
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(
      (sub) => sub.unsubscribe()
    );
  }

  private initShops() {
    this.isLoading = true;
    this.shops = this.shopsService.getShops();
  }

  public onSelectShop(shop) {
    this.shopsService.activeShop = shop;
    this.router.navigate(['home/shops-details']);
  }

  public onRemoveShop(data: {ev: any, shop: any}) {
    this.shops = <any>this.shopsService.removeShop(data.shop);
    data.ev.stopPropagation();
  }

  public onAddShop() {
    this.sidebarService.showSidebar(AddShopComponent, this.sidebar.viewContainerRef);
  }
}
