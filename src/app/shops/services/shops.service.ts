import {Injectable} from '@angular/core';
import {of, Subject} from 'rxjs';
import {ShopOwner} from '../../shop-owners/shop-owners.component';
import {ProductModel} from '../../shared/models/product.model';
import {ShopModel} from '../../shared/models/shop.model';

@Injectable()
export class ShopsService {
  public onOpenImageSlider = new Subject();
  public onAddProduct = new Subject();
  public onAddShop = new Subject();
  public activeShop;
  public activeProduct;

  constructor() {
  }

  public getShops() {
    const shops = localStorage.getItem('shops');
    const hasShops = !!shops;
    if (hasShops) {
      return JSON.parse(shops);
    } else {
      localStorage.setItem('shops', JSON.stringify([]));
      return [];
    }
  }

  public removeShop(targetShop) {
    let shops = this.getShops();
    shops = shops.filter(shop => shop.name !== targetShop.name);
    localStorage.setItem('shops', JSON.stringify(shops));
    return shops;
  }

  public addShop(shop) {
    const shops = this.getShops();
    shops.push({
      backgroundImage: shop.imgURL,
      name: shop.name,
      products: [],
      owner: shop.owner
    });
    localStorage.setItem('shops', JSON.stringify(shops));
    this.onAddShop.next();
  }

  public getOwners(): ShopOwner[] {
    const shops = this.getShops();
    const owners = [];
    shops.forEach(shop => owners.push(shop.owner));
    return owners;
  }

  public removeOwner(owner) {
    const shops = this.getShops();
    const newShops = [];
    const owners = [];
    shops.forEach(shop => {
      if (owner.name !== shop.owner.name) {
        newShops.push(shop);
        owners.push(shop.owner);
      }
    });
    localStorage.setItem('shops', JSON.stringify(newShops));
    return owners;
  }


  public addProduct(product) {
    const shops = this.getShops();
    const shopOwner = shops.find((shop) => {
      return this.activeShop.name === shop.name;
    });
    shopOwner.products.push({
      owner: this.activeShop.name,
      rating: 1.5,
      ...product
    });
    this.activeShop = shopOwner;
    localStorage.setItem('shops', JSON.stringify(shops));
    this.onAddProduct.next();
  }

  public addOwner(owner) {
    const shops: ShopModel[] = this.getShops();
    shops.push({
      name: owner.shopName,
      backgroundImage: owner.shopImgURL,
      products: [],
      owner: {
        name: owner.name,
        imgURL: owner.imgURL,
        email: owner.email,
        password: owner.password,
        id: Math.random() * 10000000000
      }
    });
    localStorage.setItem('shops', JSON.stringify(shops));
    this.onAddShop.next();
  }

  public getUsers() {
    const users = JSON.parse(localStorage.getItem('users'));
    if (users && users instanceof Array) {
      return users;
    }
    return [];
  }
}
