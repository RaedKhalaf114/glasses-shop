import {NgModule} from '@angular/core';
import {ShopsComponent} from './shops.component';
import {ShopsService} from './services/shops.service';
import {ProductWearComponent} from './product-wear/product-wear.component';
import {ProductComponent} from './product-item/product.component';
import {ShopDetailsComponent} from './shop-details/shop-details.component';
import {ShopItemComponent} from './shop-item/shop-item.component';
import {SharedModule} from '../shared/modules/shared.module';
import {SharedComponentsModule} from '../shared/components/components.index';
import {DialogService} from 'primeng/api';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {BasketComponent} from './basket/basket.component';
import {ScreenShotsComponent} from './screen-shots/screen-shots.component';
import {SidebarModule} from 'primeng/primeng';
import {ImageSliderComponent} from './image-slider/image-slider.component';
import {ToastModule} from 'primeng/toast';
import {RatingComponent} from './rating/rating.component';
import {AddShopComponent} from './add-shop/add-shop.component';
import {AddProductComponent} from './add-product/add-product.component';

@NgModule({
  declarations: [
    ShopsComponent,
    ShopItemComponent,
    ShopDetailsComponent,
    ProductComponent,
    ProductWearComponent,
    BasketComponent,
    ScreenShotsComponent,
    ImageSliderComponent,
    RatingComponent,
    AddShopComponent,
    AddProductComponent
  ],
  exports: [
    ShopsComponent
  ],
  imports: [
    SharedModule,
    SharedComponentsModule,
    DynamicDialogModule,
    SidebarModule,
    ToastModule
  ],
  providers: [
    ShopsService,
    DialogService
  ],
  entryComponents: [
    ProductWearComponent,
    ScreenShotsComponent,
    BasketComponent,
    ImageSliderComponent,
    AddShopComponent,
    AddProductComponent
  ]
})
export class ShopsModule {
}
