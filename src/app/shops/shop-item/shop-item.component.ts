import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ShopModel} from '../../shared/models/shop.model';
import {AuthenticationService} from '../../shared/services/authentication.service';

@Component({
  selector: 'enigma-shop-item-component',
  templateUrl: 'shop-item.component.html',
  styleUrls: ['shop-item.component.css']
})
export class ShopItemComponent {
  @Input() shop: ShopModel;
  @Output() removeShop = new EventEmitter();
  user;

  constructor(
    private authService: AuthenticationService
  ) {
    this.user = this.authService.getUser();
  }

  canDispolayRemove() {
    try {
      return this.user.email === this.shop.owner.email;
    } catch (e) {
      return false;
    }
  }
}
