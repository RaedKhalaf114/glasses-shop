import {Component, Inject} from '@angular/core';
import {Sidebar} from '../../shared/behavior/sidebar';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ShopsService} from '../services/shops.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'enigma-add-shop-component',
  styleUrls: ['add-shop.component.css'],
  templateUrl: 'add-shop.component.html'
})
export class AddShopComponent extends Sidebar {
  addShopForm: FormGroup;
  isSubmitting = false;
  shopOwners = [];

  constructor(
    @Inject(FormBuilder) private formBuilder: FormBuilder,
    private shopsService: ShopsService,
    private messageService: MessageService
  ) {
    super();
  }

  onSidebarInit() {
    this.initForm();
    this.initShopOwners();
  }

  private initShopOwners() {
    this.shopOwners = this.shopsService.getOwners()
      .map((owner) => ({label: owner.name, value: owner}));
    if (this.shopOwners && this.shopOwners.length === 1) {
      this.addShopForm.controls['owner'].setValue(this.shopOwners[0].value);
      this.addShopForm.controls['owner'].updateValueAndValidity();
    }
  }

  private initForm() {
    this.addShopForm = this.formBuilder.group({
      name: ['', Validators.required],
      imgURL: ['', Validators.required],
      owner: [null, Validators.required]
    });
    this.addShopForm.controls['name'].valueChanges.subscribe((value) => {
      console.log(this);
    });
  }

  public onAddShop() {
    this.isSubmitting = true;
    setTimeout(() => {
      this.shopsService.addShop(this.addShopForm.getRawValue());
      this.onHide();
      this.messageService.add({severity: 'success', summary: 'Shop added Successfully'});
    }, 2000);
  }
}
