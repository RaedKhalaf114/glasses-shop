import {ChangeDetectorRef, Component, ComponentFactoryResolver, ViewChild} from '@angular/core';
import {Sidebar} from '../../shared/behavior/sidebar';
import {ScreenShotService} from '../../shared/services/screen-shot.service';
import {ShopsService} from '../services/shops.service';
import {ImageSliderComponent} from '../image-slider/image-slider.component';
import {SideBarDirective} from '../../shared/components/directives/side-bar.directive';
import {AuthenticationService} from '../../shared/services/authentication.service';

@Component({
  selector: 'enigma-screen-shots-component',
  templateUrl: 'screen-shots.component.html',
  styleUrls: ['screen-shots.component.css']
})
export class ScreenShotsComponent extends Sidebar {
  public screenShots;
  @ViewChild(SideBarDirective) imageSlider: SideBarDirective;

  constructor(
    private screenShotsService: ScreenShotService,
    private shopsService: ShopsService,
    private componentFactory: ComponentFactoryResolver,
    private chRef: ChangeDetectorRef,
    private authService: AuthenticationService
  ) {
    super();
  }

  onSidebarInit() {
    this.initScreenShots();
  }

  public initScreenShots() {
    this.screenShots = this.screenShotsService.screenshots.filter((screen) => screen.user.email === this.authService.getUser().email);
  }

  public openImage(i) {
    this.showImagesSlider({images: this.screenShots, activeIndex: i});
  }

  public removeScreenShot($event, index) {
    $event.stopPropagation();
    this.screenShotsService.removeScreenshot(index);
    this.screenShots = this.screenShotsService.getScreenShots();
  }


  public showImagesSlider(data) {
    const componentFactory = this.componentFactory.resolveComponentFactory(ImageSliderComponent);

    const containerRef = this.imageSlider.viewContainerRef;
    containerRef.clear();

    const componentRef = containerRef.createComponent(componentFactory);
    componentRef.instance.images = data.images;
    componentRef.instance.activeIndex = data.activeIndex;
    componentRef.instance.closeSlider = () => containerRef.clear();
    this.chRef.detach();
  }
}
