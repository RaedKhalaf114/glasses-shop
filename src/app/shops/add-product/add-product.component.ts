import {Component, Inject} from '@angular/core';
import {Sidebar} from '../../shared/behavior/sidebar';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ShopsService} from '../services/shops.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'enigma-add-product-component',
  templateUrl: 'add-product.component.html',
  styleUrls: ['add-product.component.css']
})
export class AddProductComponent extends Sidebar {
  form: FormGroup;
  isSubmitting;

  constructor(
    @Inject(FormBuilder) private formBuilder: FormBuilder,
    private shopsService: ShopsService,
    private messageService: MessageService
  ) {
    super();
  }

  onSidebarInit() {
    this.initForm();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      imgURL: ['', Validators.required],
      price: [0, Validators.required],
      productId: [null, Validators.required]
    });
  }

  public onAddProduct() {
    this.isSubmitting = true;
    setTimeout(() => {
      this.onHide();
      this.shopsService.addProduct(this.form.getRawValue());
      this.messageService.add({severity: 'success', summary: 'Product added Successfully'});
    }, 2000);
  }
}

