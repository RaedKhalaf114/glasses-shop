import {Component, Input} from '@angular/core';
import {Sidebar} from '../../shared/behavior/sidebar';
import {ProductModel} from '../../shared/models/product.model';
import {BasketService} from '../../shared/services/basket.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'enigma-basket-component',
  templateUrl: 'basket.component.html',
  styleUrls: ['basket.component.css']
})
export class BasketComponent extends Sidebar {
  @Input() onClose;
  public basketProducts: ProductModel[] = [];
  public isLoading = false;

  constructor(
    private basketService: BasketService,
    private messageService: MessageService
  ) {
    super();
  }

  onSidebarInit() {
    this.initBasketItems();
  }

  public initBasketItems() {
    this.basketProducts = this.basketService.getBasketItems();
  }

  onOrder() {
    this.isLoading = true;
    setTimeout(() => {
      this.onClose({severity: 'success', summary: 'Ordered Successfully'});
      this.basketService.emptyBasket();
    }, 2000);
  }

  public removeProduct(index) {
    this.basketService.removeBasketProduct(index);
    this.basketProducts = this.basketService.getBasketItems();
  }
}
