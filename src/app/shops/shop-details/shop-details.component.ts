import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FADE_ANIMATION} from '../../shared/animations/fade-animation';
import {ShopModel} from '../../shared/models/shop.model';
import {ShopsService} from '../services/shops.service';
import {Router} from '@angular/router';
import {SideBarDirective} from '../../shared/components/directives/side-bar.directive';
import {SidebarService} from '../../shared/services/sidebar-service';
import {AddShopComponent} from '../add-shop/add-shop.component';
import {AddProductComponent} from '../add-product/add-product.component';

@Component({
  selector: 'enigma-shop-details-component',
  templateUrl: 'shop-details.component.html',
  styleUrls: ['shop-details.component.css'],
  animations: [FADE_ANIMATION]
})
export class ShopDetailsComponent implements OnInit, OnChanges, OnDestroy {
  shop: ShopModel;
  @Output() back: EventEmitter<any> = new EventEmitter();
  @ViewChild(SideBarDirective) sidebar: SideBarDirective;
  subscriptions = [];
  user;

  constructor(
    private shopsService: ShopsService,
    private router: Router,
    private sidebarService: SidebarService
  ) {
  }

  ngOnChanges() {
  }

  onBack() {
    this.back.emit();
  }

  ngOnInit() {
    this.shop = this.shopsService.activeShop;
    const storedShop = localStorage.getItem('reloadedShop');
    if (this.shop) {
    } else if (storedShop) {
      this.shop = JSON.parse(storedShop);
      localStorage.removeItem('reloadedShop');
      this.shopsService.activeShop = this.shop ;
    } else {
      this.router.navigate(['home/shops']);
    }
    this.lsnToProductChange();
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  onAddProduct() {
    this.sidebarService.showSidebar(AddProductComponent, this.sidebar.viewContainerRef);
  }

  public lsnToProductChange() {
    this.subscriptions.push(this.shopsService.onAddProduct.subscribe(_ => this.shop = this.shopsService.activeShop));
  }

  ngOnDestroy() {
    this.subscriptions.forEach($sbcr => $sbcr.unsubscribe());
  }

  canAddShop() {
    try {
      return this.user.email === this.shop.owner.email;
    } catch (e) {
      return false;
    }
  }
}
