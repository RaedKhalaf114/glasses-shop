import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ScreenShotService} from '../../shared/services/screen-shot.service';
import {ShopsService} from '../services/shops.service';
import {AuthenticationService} from '../../shared/services/authentication.service';


declare const main: any;
const resizebase64 = require('resize-base64');

@Component({
  selector: 'enigma-product-wear-component',
  templateUrl: 'product-wear.component.html',
  styleUrls: ['product-wear.component.css']
})
export class ProductWearComponent implements OnInit, OnDestroy {
  @ViewChild('canvasElement') canvasEl;
  screenshot;

  constructor(
    private screenShotService: ScreenShotService,
    private shopsService: ShopsService,
    private authservice: AuthenticationService
  ) {}

  ngOnInit() {
    main(this.shopsService.activeProduct.productId);
  }

  ngOnDestroy(): void {
    location.reload();
    localStorage.setItem('reloadedShop', JSON.stringify(this.shopsService.activeShop));
  }

  onSaveScreenshot() {
    this.screenshot = this.canvasEl.nativeElement.toDataURL();
    try {
      this.screenShotService.storeScreenShot
      ({imgSrc: resizebase64(this.screenshot), product: this.shopsService.activeProduct, user: this.authservice.getUser()});
    } catch (e) {
      this.screenShotService.storeScreenShot
      ({imgSrc: this.screenshot, product: this.shopsService.activeProduct, user: this.authservice.getUser()});
    }
    setTimeout(() => this.screenshot = null, 2000);
  }
}
