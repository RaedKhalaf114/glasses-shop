import {ChangeDetectorRef, Component, Input, OnChanges, OnInit} from '@angular/core';
import {BasketService} from '../../shared/services/basket.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'enigma-image-slider-component',
  templateUrl: 'image-slider.component.html',
  styleUrls: ['image-slider.component.css']
})
export class ImageSliderComponent {
  @Input() images;
  @Input() activeIndex;
  @Input() closeSlider;

  constructor(
    private basketService: BasketService,
    private chRef: ChangeDetectorRef
  ) {
    setTimeout(() => chRef.detectChanges(), 0);
  }

  downloadImage() {
    saveAs(this.images[this.activeIndex].imgSrc, 'screen-shot.png');
  }

  addToBasket() {
    this.basketService.addToBasket(this.images[this.activeIndex].product);
  }

  goLeft() {
    this.activeIndex--;
    if (this.activeIndex < 0) {
      this.activeIndex = this.images.length - 1;
    }
    setTimeout(() => this.chRef.detectChanges(), 0);
  }

  goRight() {
    this.activeIndex++;
    if (this.activeIndex === this.images.length) {
      this.activeIndex = 0;
    }
    setTimeout(() => this.chRef.detectChanges(), 0);
  }
}
