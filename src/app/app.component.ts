import {Component, OnInit} from '@angular/core';
import {LanguageService} from './shared/services/language.service';

@Component({
  selector: 'enigma-root-component',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private languageService: LanguageService
  ) {}

  ngOnInit() {
    this.initLanguage();
  }

  private initLanguage() {
    this.languageService.initLanguage();
  }
}
