import {Component} from '@angular/core';

@Component({
  selector: 'enigma-authentication-component',
  templateUrl: 'authentication.component.html',
  styleUrls: ['authentication.component.css']
})
export class AuthenticationComponent {}
