import {NgModule} from '@angular/core';
import {LoginComponent} from './login-component/login.component';
import {LoginService} from './services/login.service';
import {SharedComponentsModule} from '../shared/components/components.index';
import {SharedModule} from '../shared/modules/shared.module';
import {CheckboxModule} from 'primeng/checkbox';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {AuthenticationComponent} from './authentication.component';
import {RouterModule} from '@angular/router';
import {ToastModule} from 'primeng/toast';

@NgModule({
  declarations: [
    AuthenticationComponent,
    LoginComponent,
    ForgotPasswordComponent
  ],
  exports: [
    AuthenticationComponent
  ],
  imports: [
    SharedModule,
    RouterModule,
    SharedComponentsModule,
    CheckboxModule,
    ToastModule
  ],
  providers: [
    LoginService
  ]
})
export class AuthenticationModule {}
