import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {FADE_ANIMATION} from '../../shared/animations/fade-animation';
import {LoginService} from '../services/login.service';
import {AuthenticationService} from '../../shared/services/authentication.service';
import {LoginHttpResponseModel} from '../models/http-response/login-response.model';
import {ShopsService} from '../../shops/services/shops.service';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'enigma-login-component',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css'],
  animations: [FADE_ANIMATION],
})
export class LoginComponent implements OnInit {
  /**
   * Login Form.
   * @type {string}
   */
  public loginForm: FormGroup;

  /**
   * show Spinner which submitting.
   * @type {boolean}
   */
  public isLoading = false;

  public showAlertMessage = false;
  public alertMessage;

  constructor(
    @Inject(FormBuilder) private formBuilder: FormBuilder,
    private loginService: LoginService,
    private authService: AuthenticationService,
    private router: Router,
    private shopsService: ShopsService,
    public  afAuth:  AngularFireAuth
  ) {}


  ngOnInit() {
    this.checkIfAuthenticated();
    this.initForm();
  }

  /**
   * If use is active,
   * return to home page.
   */
  private checkIfAuthenticated() {
    if (this.authService.isUserActive()) {
      // this.router.navigate(['home']);
    }
  }

  private initForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      rememberMe: [false]
    });
  }

  public onSignIn() {
    if (!this.loginForm.valid) {
      return;
    }

    this.isLoading = true;

    const requestBody = this.loginForm.getRawValue();

    delete requestBody.rememberMe;

    this.afAuth.auth.signInWithEmailAndPassword(requestBody.username, requestBody.password).then().catch();
    setTimeout(() => {
      switch (requestBody.username.toLocaleLowerCase().trim()) {
        // Admin role
        case 'admin@gmail.com':
          this.authService.setUserAuthToken('test-token');
          this.authService.setUser({
            name: 'Admin user',
            role: 'admin',
            email: 'admin@gmail.com',
            token: 'test-token',
            avatar: 'https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1',
            id: Math.random() * 10000000000
          });
          this.router.navigate(['/home']);
          break;
        case 'user@gmail.com':
          this.authService.setUserAuthToken('test-token');
          this.authService.setUser({
            name: 'User',
            email: 'user@gmail.com',
            role: 'user',
            token: 'test-token',
            avatar: 'https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1',
            id: Math.random() * 10000000000
          });
          this.router.navigate(['/home']);
          break;
        case 'shop-owner@gmail.com':
          this.authService.setUserAuthToken('test-token');
          this.authService.setUser({
            name: 'Shop Owner',
            role: 'owner',
            email: 'shop-owner@gmail.com',
            token: 'test-token',
            avatar: 'https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1',
            id: Math.random() * 10000000000
          });
          this.router.navigate(['/home']);
          break;
        default:
          const LoggedUser = this.checkLocalStorageUsers(requestBody);
          if (LoggedUser ) {
            this.authService.setUserAuthToken('test-token');
            this.authService.setUser(LoggedUser);
            this.router.navigate(['/home']);
            break;
          }
          this.isLoading = false;
          this.showAlertMessage = true;
          this.alertMessage = 'Wrong Username or password';
      }
    }, 1500);
  }

  public checkLocalStorageUsers(form) {
    const users = JSON.parse(localStorage.getItem('users'));
    const owners = this.shopsService.getOwners();
    let activeUser = null;
    if (users && users instanceof Array) {
      users.some(
        (user) => {
          if (user.email === form.username && user.password === form.password) {
            activeUser = user;
            return true;
          }
          return false;
        }
      );
    }

    if (owners && owners instanceof Array && !activeUser) {
      owners.some(
        (user) => {
          if (user.email === form.username && user.password === form.password) {
            activeUser = user;
            return true;
          }
          return false;
        }
      );
    }
    return activeUser;
  }

  public openForgotPassword() {
    this.router.navigate(['auth/register']);
  }
}
