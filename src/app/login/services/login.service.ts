import {Injectable} from '@angular/core';
import {LoginHttpRequestModel} from '../models/http-requests/login-request.model';
import {Observable, of} from 'rxjs';
import {LoginHttpResponseModel} from '../models/http-response/login-response.model';

@Injectable()
export class LoginService {

  constructor(
  ) {}

  public login(reqBody: LoginHttpRequestModel): Observable<LoginHttpResponseModel> {
    // TODO Interact with Login API.
    return of<LoginHttpResponseModel>({token: 'testing'});
  }
}
