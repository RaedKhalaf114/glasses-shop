import {Component, Inject, OnInit} from '@angular/core';
import {FADE_ANIMATION} from '../../shared/animations/fade-animation';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../shared/services/authentication.service';

@Component({
  selector: 'enigma-forgot-password-component',
  templateUrl: 'forgot-password.component.html',
  styleUrls: ['forgot-password.component.css'],
  animations: [FADE_ANIMATION]
})
export class ForgotPasswordComponent implements OnInit {
  /**
   * Login Form.
   * @type {string}
   */
  public forgotPasswordForm: FormGroup;

  /**
   * show Spinner which submitting.
   * @type {boolean}
   */
  public isLoading = false;

  constructor(
    @Inject(FormBuilder) private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthenticationService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', Validators.required],
      name: ['', Validators.required],
      password: ['', Validators.required],
      imgURL: ['', Validators.required]
    });
  }

  public submit() {
    if (!this.forgotPasswordForm.valid) {
      return;
    }

    this.isLoading = true;

    setTimeout(() => {
      this.authService.addUser(this.forgotPasswordForm.getRawValue());
      this.authService.setUserAuthToken('test token');
      this.router.navigate(['/home']);
    }, 3000);
  }

  public openSignIn() {
    this.router.navigate(['auth/login']);
  }
}
