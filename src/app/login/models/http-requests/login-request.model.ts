export interface LoginHttpRequestModel {
  username: string;
  password: string;
}
