import {Routes} from '@angular/router';
import {LoginComponent} from './login-component/login.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';

export const AUTH_ROUTS: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: ForgotPasswordComponent
  }
];
